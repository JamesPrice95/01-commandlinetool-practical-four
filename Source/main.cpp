//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.h"

bool testLinkedList();

template <typename Type>
Type max (Type a, Type b);

template <typename Type>
Type min (Type a, Type b);

template <typename Type>
Type add (Type a, Type b);

template <typename Type>
Type print (Type array[], Type size);

template <typename Type>
Type getAverage (Type array[], Type size);

int main (int argc, const char* argv[])
{
    LinkedList list;
   // float value;
  //  std::cin >> value;
    
    double array[10];
    double total = 0;
    
    for(int i; i<10; i ++)
    {
        array[i] = i;
    }
    
    total = getAverage(array[10], 10);
    
    std::cout << total;
    
    //bool state;
    
   //state = testLinkedList();
    
    //std::cout << state;
    return 0;
}

bool testLinkedList()
{
    LinkedList list;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);

    if (list.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        list.add (testArray[i]);
        for (int y = 0; y < list.size(); y++)
        {
            std::cout << list.get(y) << "\n";
        }
        if (list.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (list.get (i) != testArray[i])
       {
           std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    return true;

}

template <typename Type>
Type max (Type a, Type b)
{
    if (a < b)
        return b;
    
    else
        return a;
    
    
}

template <typename Type>
Type min (Type a, Type b)
{
    if (a > b)
        return b;
    
    else
        return a;
    
}

template <typename Type>
Type add (Type a, Type b)
{
    
    Type c = a + b;
    
    return c;
    
}

template <typename Type>
Type print (Type array[], Type size)
{
    for(int i; i < size; i++)
    {
        std::cout << array[i];
    }
}

template <typename Type>
Type getAverage (Type array[], Type size)
{
    double sum = 0;
    
    for(int i; i < size; i++)
    {
        sum = sum + array[i];
    
    }
    return sum;
    
}
