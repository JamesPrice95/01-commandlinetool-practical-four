//
//  array.h
//  
//
//  Created by James Price on 06/10/2016.
//
//

#ifndef ____array__
#define ____array__

#include <stdio.h>

class Array
{
public:
    
    Array(); /* Constructer */
    
    ~Array(); /* Destructer */
    
    void add (float itemValue); /* adds a new value to the end of the array */
    
    float get (int index); /* returns the value at index */
    
    int size(); /* returns number of items in array */
    
private:
    
    int arraySize;
    float *floatPoint;
};






#endif /* defined(____array__) */
